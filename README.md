git_submodule_encryption
========================

simple script to encrypt git submodules, this is based on this [article](https://gist.github.com/shadowhand/873637)


##Introduction

As many developper who want to publish opensource repos, I faced the problem of having sensitive data (private keys and passwords) embedded inside the project code (I work with django and it's great ;-) ). So, wanting to share my work and ease the tasks of other developpers who will fork the repos I came with is adapatation of the repo encryption solution.


The main idea is to have a submodule in your git repo pointing to a secondary repo (you can add extra security by making it private if you want), and telling git to use some filters (smudge and clean) for this repo processing via the addition of [.gitattribute](http://git-scm.com/book/en/Customizing-Git-Git-Attributes) in its folder.


##Steps

###Some initializations
Just creating a folder to store the various encryption scripts and keys


```
mkdir ~/.gitencrypt
cd ~/.gitencrypt
touch clean_filter_openssl smudge_filter_openssl diff_filter_openssl
chmod 755 *
```


###generate an encryption key
This may be unecessary as you already have one or choose to use password/salt based approch. Either way, I'll leave this here:

```
openssl genrsa -out private.pem 2048
openssl rsa -in private.pem -out public.pem -outform PEM -pubout
```

**Note:**

>Don't forget to update the encryption scripts and point them on your own keys if you choose not use these ones


###Create the filter scripts

```
cat << EOF >> clean_filter_openssl
#!/bin/bash

# SALT_FIXED=<your-salt> # 24 or less hex characters
# PASS_FIXED=<your-passphrase>

# openssl enc -base64 -aes-256-ecb -S $SALT_FIXED -k $PASS_FIXED
openssl rsautl -encrypt -inkey ~/.gitencrypt/public.pem -pubin
EOF

cat << EOF >> smudge_filter_openssl

#!/bin/bash

# No salt is needed for decryption.
PASS_FIXED=<your-passphrase>

# If decryption fails, use `cat` instead.
# Error messages are redirected to /dev/null.
# openssl enc -d -base64 -aes-256-ecb -k $PASS_FIXED 2> /dev/null || cat
openssl rsautl -decrypt -inkey ~/.gitencrypt/public.pem -pubin 2> /dev/null || cat
EOF


cat << EOF >> diff_filter_openssl
#!/bin/bash

# No salt is needed for decryption.
# PASS_FIXED=<your-passphrase>

# Error messages are redirect to /dev/null.
openssl rsautl -decrypt -inkey ~/.gitencrypt/public.pem -pubin 2> /dev/null || cat "$1"
# openssl enc -d -base64 -aes-256-ecb -k $PASS_FIXED -in "$1" 2> /dev/null || cat "$1"
EOF
```

**Note:**

>I kept the original chain block encryption based method just in case you wanted to use it. If you choose to stick to the original author encryption you may consider reading this about the block encryption algorithms:
>
>* http://stackoverflow.com/questions/1220751/how-to-choose-an-aes-encryption-mode-cbc-ecb-ctr-ocb-cfb
>* http://en.wikipedia.org/wiki/Block_cipher_mode_of_operation


###Create a submodule in your project
Once the secondary repo ready you simply have to type


```
git submodule add [git repo] [folder name]
```

in my case I was working on this [repo](https://github.com/Chedi/CarSharing) and the secret one is [here](https://github.com/Chedi/Carsharing_secret), and I wanted to the repo under a the [secret folder](https://github.com/Chedi/CarSharing/tree/master/Mitfahrgelegenheit/settings). So my command was


```
git submodule add git@github.com:Chedi/Carsharing_secret.git secret
```

###update the submodule attributes
Now move to the new secret folder and add the declaration of the filters


```
cd secret
cat << EOF >> .gitattributes
* filter=openssl diff=openssl
[merge]
    renormalize = true
EOF
```

**Note:**

>git will complain about invalid name if by doing copy/past the spaces in the normalize line are converted to tab, they need to be four spaces.

Finally, you will need to enable the filters on you git submodule configuration. so go to the git repository root and edit the config file under .git/modules/[path to submodule]/config (not be mistaken with .git/config), and add this at the end of the file


```
[filter "openssl"]
    smudge = ~/.gitencrypt/smudge_filter_openssl
    clean = ~/.gitencrypt/clean_filter_openssl
[diff "openssl"]
    textconv = ~/.gitencrypt/diff_filter_openssl
```


